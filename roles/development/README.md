# development

> **Notice**, requires `sudo` for all tasks which means `become: true` when including/importing the role.

## Language Servers

Managed by `nvim`, see `:help lsp` or [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig).

Also check the [lsp general documentation](https://langserver.org/#implementations-client) for different Language Servers

## Role Variables

* `development_user`: defaults to **ansible_user**, user that will be added to the virtualization group.
    * **ansible_user** is undefined when running task on localhost, see [Ansible issue 23530](https://github.com/ansible/ansible/issues/23530) hence a workaround was implemented to get the username from env.
* `development_k8s_kubectl_version`: kubectl version to be installed. Current value **v1.31**
    * make sure to read [Install and Set Up kubectl on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management) & [Changing The Kubernetes Package Repository](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/change-package-repository/)

## Software

> see vars directory for details on Software installed

* git
* ctags
* neovim
* golang
* nodejs-npm (for LSP servers)
* ansible core
* docker community edition
* helm
* kubectl
* virtualization (libvirt)
* pandoc (for beautiful documentation)

`development_user` is added to `docker` & `virtualization` group.

## Author Information

Gerardo Rosales
