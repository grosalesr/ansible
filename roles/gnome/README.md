gnome
=========

Configures Fedora Workstation to my needs, this is:

* Install:
    * from repo:
        * gnome-tweaks
        * transmission-gtk
        * [caffeine](https://github.com/eonpatapon/gnome-shell-extension-caffeine) (gnome extension)
        * [pano](https://github.com/oae/gnome-shell-pano) (gnome extension) dependencies.
        * qqc2-desktop-style (improve QT style)

    * flatpaks:
        * Gnome Extensions
        * gThumb
        * [Foliate](https://flathub.org/apps/com.github.johnfactotum.Foliate)
        * [Remmina](https://flathub.org/apps/org.remmina.Remmina)
        * [Thunderbird](https://flathub.org/apps/org.mozilla.Thunderbird)
        * [Celluloid](https://flathub.org/apps/io.github.celluloid_player.Celluloid)

    * binary:
        * [Satty](https://github.com/gabm/Satty) for screenshot annotation.

* Gnome tuning:
    * General
        * Use 24 hours clock
        * Enable Night Light
        * Sort directories first
        * Show weekday in desktop
        * Open windows in the center of the screen
        * Wait 10 minutes before turn off the screen

    * Gedit
        * Enable bracket matching
        * Display line numbers
        * Highlight current line

    * Terminal
        * [Comic Code font](https://tosche.net/fonts/comic-code), size 11, Medium weigth
        * **Block** cursor **do not** blink
        * Hide scrollbar

* Nautilus (Files) custom scripts
    * <F12> shortcut to open terminal in current directory

Requirements
------------

`common` role **might** be needed for some tasks/configuration to work properly.

Role Variables
--------------

* `gnome_user`, defaults to **ansible_user**.
    * **ansible_user** is undefined when running task on localhost, see [Ansible issue 23530](https://github.com/ansible/ansible/issues/23530) hence a workaround was implemented to get the username from env.

License
-------
GPL3 or higher

Author Information
------------------
Gerardo Rosales
