#!/bin/bash

FONT_HINTING="full"
CURSOR_THEME="oreo_red_cursors"
MONOSPACE_FONT="Source Code Pro 10" # default

set_monospace_font() {
  local custom_font="Comic Code Medium"
  local font_size="11" # for regular font size, leave blank

  fc-list | grep -i "$custom_font" > /dev/null

  if [ $? -eq 0 ]; then
    MONOSPACE_FONT="$custom_font $font_size"
  fi
}

gnome_desktop_interface() {
  gsettings set org.gnome.desktop.interface font-hinting 'full'
  gsettings set org.gnome.desktop.interface font-antialiasing 'rgba'
  gsettings set org.gnome.desktop.interface clock-format '24h'
  gsettings set org.gnome.desktop.interface clock-show-weekday 'true'
  gsettings set org.gnome.desktop.interface font-hinting "$FONT_HINTING"
  gsettings set org.gnome.desktop.interface cursor-theme "$CURSOR_THEME"
  gsettings set org.gnome.desktop.interface monospace-font-name "$MONOSPACE_FONT"

  gsettings set org.gnome.mutter center-new-windows 'true'
  gsettings set org.gnome.desktop.session idle-delay 600 # wait 10 min to turn off the screen
  gsettings set org.gtk.Settings.FileChooser sort-directories-first 'true'
  gsettings set org.gtk.gtk4.Settings.FileChooser sort-directories-first 'true'
  gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled 'true'
}

power_settings() {
  # Automatic Suspend
  # on AC, do not suspend
  gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'

  # on Battery, suspend after 2 hours (time in seconds)
  gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 7200
}

text_editor() {
  local mode="default" # vim or default

  gsettings set org.gnome.TextEditor show-map 'true'
  gsettings set org.gnome.TextEditor keybindings "$mode" # "default" for non modal editor
  gsettings set org.gnome.TextEditor restore-session 'false'
  gsettings set org.gnome.TextEditor show-line-numbers 'true'
}

ptyxis_terminal() {
  local profile="$(gsettings get org.gnome.Ptyxis default-profile-uuid | tr -d \')"

  gsettings set org.gnome.Ptyxis cursor-blink-mode 'off'
  gsettings set org.gnome.Ptyxis audible-bell 'false'
  dconf write /org/gnome/Ptyxis/Profiles/${profile}/palette 'Gruvbox'
}

gnome_terminal() {
  local profile=$(gsettings get org.gnome.Terminal.ProfilesList list | tr -d \' | tr -d [])
  #                                                                   removes ' and [] from output

  #"gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:{{ termProfile.stdout }}/ font 'Comic Code Medium 11'"
  dconf write /org/gnome/terminal/legacy/profiles:/:${profile}/font \'"$MONOSPACE_FONT"\'
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profile}/ cursor-shape 'block'
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profile}/ audible-bell 'false'
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profile}/ use-system-font 'false'
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profile}/ scrollbar-policy 'never'
  gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profile}/ cursor-blink-mode 'off'
}

main() {
  local gnome_terminal_path="$(whereis -b gnome-terminal | cut -d: -f2)"
  local ptyxis_terminal_path="$(whereis -b ptyxis | cut -d: -f2 )"

  set_monospace_font

  gnome_desktop_interface
  power_settings
  text_editor

  printf "Configure "
  if [ ! -z $gnome_terminal_path ]; then
    printf "Gnome Terminal\n"
    gnome_terminal
  fi

  if [ ! -z $ptyxis_terminal_path ]; then
    printf "Ptyxis Terminal\n"
    ptyxis_terminal

  fi
}

main
