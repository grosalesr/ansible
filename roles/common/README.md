# common

> **Notice**, requires `sudo` for all tasks which means `become: true` when including/importing the role.

Configures Fedora to my needs, this is:

## Repositories

- Add [RPM Fusion](https://rpmfusion.org/) repository
- Enable [Workstation Repository](https://docs.fedoraproject.org/en-US/workstation-working-group/third-party-repos/)
- Vanilla [Flathub](https://flathub.org/) remote
    - Fedora's remotes & curated Flathub are removed.

## System Config

- Laptop lid behavior configuration through systemd
- Install [Oreo cursors](https://www.pling.com/p/1360254/)
- Hardware acceleration for intel or AMD GPUs

## Software

> see vars directory for details on Software installed/removed

- Google Chrome
- system packages to provide OS functionality
- multimedia packages, as per [RPM Fusion](https://rpmfusion.org/Howto/Multimedia?highlight=%28%5CbCategoryHowto%5Cb%29) & [Fedora docs](https://docs.fedoraproject.org/en-US/quick-docs/assembly_installing-plugins-for-playing-movies-and-music/)
- Flatpak applications
- Fully update the distribution
- Reboots the target machine
